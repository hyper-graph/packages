import {
	BaseExecutionMode,
	ExecutionModeVariants,
} from './base.execution.mode';

export class HgExecutionMode extends BaseExecutionMode {
	protected getDefaultValue(): ExecutionModeVariants {
		return ExecutionModeVariants.PROD;
	}

	protected isValueValid(value = ''): value is ExecutionModeVariants {
		return Object.values<string>(ExecutionModeVariants).includes(value);
	}

	protected getEnvName(): string {
		return 'HG_ENV';
	}
}
