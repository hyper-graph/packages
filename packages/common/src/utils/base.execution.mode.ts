export enum ExecutionModeVariants {
	DEBUG = 'debug',
	DEV = 'dev',
	TEST = 'test',
	QA = 'qa',
	PROD = 'prod',
}

export abstract class BaseExecutionMode<ValueType extends ExecutionModeVariants = ExecutionModeVariants> {
	private value: ValueType;

	public constructor() {
		this.initValue();
	}

	public isDebug(): boolean {
		return this.value === ExecutionModeVariants.DEBUG;
	}

	public isDev(): boolean {
		return this.value === ExecutionModeVariants.DEV;
	}

	public isTest(): boolean {
		return this.value === ExecutionModeVariants.TEST;
	}

	public isQa(): boolean {
		return this.value === ExecutionModeVariants.QA;
	}

	public isProd(): boolean {
		return this.value === ExecutionModeVariants.PROD;
	}

	public getValue(): ValueType {
		return this.value;
	}

	public isValueIn(variants: ValueType[]): boolean {
		return variants.includes(this.value);
	}

	public is(expected: ValueType): boolean {
		return expected === this.value;
	}

	protected abstract getEnvName(): string;
	protected abstract isValueValid(value?: string): value is ValueType;
	protected abstract getDefaultValue(): ValueType;

	private initValue(): void {
		const value = this.getRawValue();

		if (this.isValueValid(value)) {
			this.value = value;
		} else {
			this.value = this.getDefaultValue();
		}
	}

	private getRawValue(): string | undefined {
		return process.env[this.getEnvName()];
	}
}
