/* eslint-disable @typescript-eslint/prefer-ts-expect-error */
declare global {
	// @ts-ignore
	type Nullable<T> = T | null;
	// @ts-ignore
	type Optional<T> = T | undefined;
	// @ts-ignore
	type Class<Instance = any, Argument extends any[] = any[]> = new (...args: Argument) => Instance;
}

export {};
