export abstract class BaseEntity<IdType = string> {
	protected readonly id: IdType;

	protected constructor(id: IdType) {
		this.id = id;
	}
}
