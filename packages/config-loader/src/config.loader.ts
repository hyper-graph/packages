import type { BaseExecutionMode } from '@hg/common';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import fs from 'fs/promises';
import { resolve } from 'path';
import * as process from 'process';
import { getAllMetadata } from './enforce-env.decorator';

type LoadOptions = {

	/**
	 *
	 * Override default app path
	 * @default process.cwd()
	 */
	appPath?: string;

	/**
	 * Config file name
	 */
	configName: string;

	/**
	 *
	 * Additional directory to appPath
	 * @default null
	 */
	configDir?: Nullable<string>;

	/**
	 *
	 * By default use environment variable for build config.
	 * Get base config, apply ${env} config and, after, apply local config
	 * @default true
	 */
	disableEnvBuilder?: boolean;

	/**
	 *
	 * Override injected env
	 */
	overrideEnv?: Nullable<BaseExecutionMode>;

	/**
	 *
	 * Used for loading root package config. Useful with overridePostfix option
	 * @default null
	 * @todo Implement
	 */
	basePostfix?: Nullable<string>;

	/**
	 *
	 * Useful with basePostfix.
	 * For example:
	 * You can specify configName to 'hg', basePostfix to 'config' and overridePostfix to 'override'.
	 * In this case will be loaded 'bg.config.{ext}' and overrode by 'hg.override.{ext}'.
	 * Similar scheme used by many tools like 'docker-compose'.
	 * @default null
	 * @todo Implement
	 */
	overridePostfix?: Nullable<string>;

	/**
	 * Apply all configs from `appDir` to `/` until `root` field won't be specified
	 * @default false
	 * @todo Implement
	 */
	recursive?: boolean;
};

export class ConfigLoader {
	private readonly options: Required<LoadOptions>;

	public constructor(options: LoadOptions) {
		this.options = {
			appPath: process.cwd(),
			configDir: null,
			disableEnvBuilder: false,
			overrideEnv: null,
			overridePostfix: null,
			basePostfix: null,
			recursive: false,
			...options,
		};
	}
	public async load<ConfigType extends object>(ctor: Class<ConfigType>): Promise<ConfigType> {
		const rawConfig = await this.loadRawConfig();

		const instance = this.transform(rawConfig, ctor);

		await this.validate(instance);

		return instance;
	}

	private async loadRawConfig(): Promise<unknown> {
		const configs = await Promise.all([
			this.loadBaseConfig(),
			this.loadEnvConfig(),
			this.loadLocalConfig(),
			this.loadOverrideConfig(),
		]);

		return this.mergeConfigs(configs);
	}

	private transform<ConfigType extends object>(rawConfig: unknown, ctor: Class<ConfigType>): ConfigType {
		const metadata = getAllMetadata(ctor.prototype);

		metadata.forEach((enforceEnv, field) => {
			if (process.env[enforceEnv]) {
				(rawConfig as Record<string | symbol, unknown>)[field as any] = process.env[enforceEnv];
			}
		});
		return plainToClass(ctor, rawConfig);
	}

	private async validate<ConfigType extends object>(config: ConfigType): Promise<void> {
		const validationErrors = await validate(config);

		if (validationErrors.length > 0) {
			// eslint-disable-next-line lines-around-comment
			/*
			 * TODO: Use aggregate exception from @hg/exception
			 * TODO: Map validateErrors to BaseException from @hg/exception
			 */
			throw new AggregateError(validationErrors, 'Config validation error');
		}
	}

	private async loadBaseConfig(): Promise<unknown> {
		const { appPath, configDir, configName, disableEnvBuilder } = this.options;
		const pathParts = [
			appPath,
			configDir,
			disableEnvBuilder ? null : 'base',
			`${configName}.json`,
		];

		return this.loadConfigFile(this.buildPathToConfig(pathParts));
	}

	private async loadEnvConfig(): Promise<unknown> {
		const { appPath, configDir, configName, overrideEnv, disableEnvBuilder } = this.options;

		if (overrideEnv === null || disableEnvBuilder) {
			return null;
		}

		const pathParts = [
			appPath,
			configDir,
			overrideEnv.getValue(),
			`${configName}.json`,
		];

		return this.loadConfigFile(this.buildPathToConfig(pathParts));
	}

	private async loadLocalConfig(): Promise<unknown> {
		const { appPath, configDir, configName, disableEnvBuilder } = this.options;

		if (disableEnvBuilder) {
			return null;
		}

		const pathParts = [
			appPath,
			configDir,
			'local',
			`${configName}.json`,
		];

		return this.loadConfigFile(this.buildPathToConfig(pathParts));
	}

	private async loadOverrideConfig(): Promise<unknown> {
		return null;
	}

	private buildPathToConfig(parts: Nullable<string>[]): string {
		const filteredParts = parts.filter((part): part is string => part !== null);

		return resolve(...filteredParts);
	}

	private mergeConfigs(configs: unknown[]): object {
		return configs
			.filter(config => config !== null)
			.filter((config): config is object => typeof config === 'object')
			.reduce((merged, next) => (
				{
					...merged,
					...next,
				}
			));
	}

	private async loadConfigFile(path: string): Promise<unknown> {
		try {
			const content = await fs.readFile(path, { encoding: 'utf-8' });

			return JSON.parse(content) as unknown;
		} catch (error: unknown) {
			return null;
		}
	}
}
