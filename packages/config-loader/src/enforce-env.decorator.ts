import 'reflect-metadata';

export const ENFORCE_ENV_METADATA = Symbol('Enforce env metadata');
export const ENFORCE_ENV_FIELDS_METADATA = Symbol('Enforce env fields metadata');

export function EnforceEnv(name: string): PropertyDecorator {
	return (target: object, propertyKey: string | symbol): void => {
		Reflect.defineMetadata(ENFORCE_ENV_METADATA, name, target, propertyKey);
		const keys = getEnforceEnvFieldsMetadata(target);
		keys.push(propertyKey);

		Reflect.defineMetadata(ENFORCE_ENV_FIELDS_METADATA, keys, target);
	};
}

export function getAllMetadata(target: object): Map<string | symbol, string> {
	const result = new Map<string | symbol, string>();

	const fields = getEnforceEnvFieldsMetadata(target);

	for (const field of fields) {
		const enforceEnv = getEnforceEnvMetadata(target, field);

		if (enforceEnv) {
			result.set(field, enforceEnv);
		}
	}

	return result;
}

function getEnforceEnvMetadata(target: object, propertyKey: string | symbol): Nullable<string> {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return Reflect.getMetadata(ENFORCE_ENV_METADATA, target, propertyKey) ?? null;
}

function getEnforceEnvFieldsMetadata(target: object): (string | symbol)[] {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return Reflect.getMetadata(ENFORCE_ENV_FIELDS_METADATA, target) ?? [];
}
