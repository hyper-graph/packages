import type { BaseEntity } from '@hg/entity';
import type { EntityNotFoundException } from '../exceptions';
import {
	BaseFindOptions,
	EntityId,
	Repository,
} from './repository';

export const INTERNAL_SYMBOL = Symbol('Repository.Internal');

type KeyType = number | string | symbol;

export type InternalData = {
	updatedKeys: Set<KeyType>;
};

export abstract class BaseRepository<Entity extends BaseEntity<any>,
	FindOptions extends BaseFindOptions<Entity> = BaseFindOptions<Entity>> extends Repository<Entity, FindOptions> {
	public async get(id: EntityId<Entity>): Promise<Nullable<Entity>> {
		// @ts-expect-error
		const [entity] = await this.find({ id });

		return entity || null;
	}

	public async getOrFail(id: EntityId<Entity>): Promise<Entity> {
		const entity = await this.get(id);

		if (entity === null) {
			throw this.getNotFoundException(id);
		}

		// eslint-disable-next-line @typescript-eslint/no-unsafe-return
		return entity;
	}

	public async has(id: EntityId<Entity>): Promise<boolean> {
		const entity = await this.get(id);

		return entity !== null;
	}

	public async find(options?: Partial<FindOptions>): Promise<Entity[]> {
		const entities = await this.findEntities(options);

		return entities.map(entity => this.enrichEntity(entity));
	}

	protected getInternalData(entity: Entity): InternalData {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-return
		return Object.getOwnPropertyDescriptor(entity, INTERNAL_SYMBOL)!.value;
	}

	protected abstract findEntities(options?: Partial<FindOptions>): Promise<Entity[]>;

	protected abstract getNotFoundException(id: EntityId<Entity>): EntityNotFoundException;

	private enrichEntity(entity: Entity): Entity {
		const initialInternalData: InternalData = { updatedKeys: new Set<string>() };

		if (!entity.hasOwnProperty(INTERNAL_SYMBOL)) {
			Object.defineProperty(entity, INTERNAL_SYMBOL, {
				value: initialInternalData,
				enumerable: false,
			});
		}

		return new Proxy<Entity>(entity, {
			// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
			set: (target: Entity, name, value) => {
				if (name !== INTERNAL_SYMBOL) {
					this.handleUpdateEntityHook(this.getInternalData(target), name, value);
				}

				return true;
			},
		});
	}

	private handleUpdateEntityHook(internalData: InternalData, name: KeyType, _value: any): void {
		internalData.updatedKeys.add(name);
	}
}
