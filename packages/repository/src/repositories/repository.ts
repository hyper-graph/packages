import type { BaseEntity } from '@hg/entity';

export type EntityId<Entity> = Entity extends BaseEntity<infer X> ? X : never;

export type BaseFindOptions<Entity extends BaseEntity<any>> = {
	id?: EntityId<Entity> | EntityId<Entity>[];
};

export abstract class Repository<Entity extends BaseEntity<any>,
	FindOptions extends BaseFindOptions<Entity> = BaseFindOptions<Entity>> {
	public abstract getNextId(): Promise<EntityId<Entity>>;

	public abstract get(id: EntityId<Entity>): Promise<Nullable<Entity>>;

	public abstract getOrFail(id: EntityId<Entity>): Promise<Entity>;

	public abstract find(options?: FindOptions): Promise<Entity[]>;

	public abstract save(entity: Entity | Entity[]): Promise<void>;

	public abstract delete(entity: Entity | Entity[]): Promise<void>;

	public abstract has(id: EntityId<Entity>): Promise<boolean>;

	public abstract clear(): Promise<void>;
}
