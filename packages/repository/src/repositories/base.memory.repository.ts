import type { BaseEntity } from '@hg/entity';
import { BaseRepository } from './base.repository';
import type {
	BaseFindOptions,
	EntityId,
} from './repository';

export abstract class BaseMemoryRepository<Entity extends BaseEntity<any>,
	FindOptions extends BaseFindOptions<Entity> = BaseFindOptions<Entity>> extends BaseRepository<Entity, FindOptions> {
	protected lastId: Nullable<EntityId<Entity>> = null;
	private readonly entitiesMap = new Map<EntityId<Entity>, Entity>();

	public async save(entity: Entity | Entity[]): Promise<void> {
		if (Array.isArray(entity)) {
			await Promise.all(entity.map(async entity => this.save(entity)));
		} else {
			this.entitiesMap.set(entity['id'], entity);
		}
	}

	public async delete(entity: Entity | Entity[]): Promise<void> {
		if (Array.isArray(entity)) {
			await Promise.all(entity.map(async entity => this.delete(entity)));
		} else {
			this.entitiesMap.delete(entity['id']);
		}
	}

	public async clear(): Promise<void> {
		this.entitiesMap.clear();
	}

	protected async findEntities(options: FindOptions): Promise<Entity[]> {
		const { id } = options;

		if (Array.isArray(id)) {
			return id.filter(id => this.entitiesMap.has(id))
				// eslint-disable-next-line @typescript-eslint/no-unsafe-return
				.map(id => this.entitiesMap.get(id)!);
		}

		if (typeof id !== 'undefined' && this.entitiesMap.has(id)) {
			return [this.entitiesMap.get(id)!];
		}

		return [];
	}
}
