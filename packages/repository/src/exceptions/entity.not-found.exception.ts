import { BaseException } from '@hg/exception';
import { format } from 'util';

export abstract class EntityNotFoundException extends BaseException {
	protected constructor(entityName: string, code = 0, id: unknown = null) {
		super(`${entityName} ${id ? `with id ${format(id)} ` : ''}not found`, code);
	}
}
