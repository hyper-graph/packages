import { v4 as uuid } from 'uuid';
import type { EntityNotFoundException } from '../exceptions';

import { BaseMemoryRepository } from '../repositories';

import type { TestEntity } from './test.entity';
import { TestNotFoundException } from './test.not-found.exception';

export class TestMemoryRepository extends BaseMemoryRepository<TestEntity> {
	public async getNextId(): Promise<TestEntity['id']> {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-return
		return uuid();
	}

	protected getNotFoundException(id: TestEntity['id']): EntityNotFoundException {
		return new TestNotFoundException(id);
	}
}
