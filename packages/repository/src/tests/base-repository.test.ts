import {
	Describe,
	expect,
	ExpectError,
	Test,
	TestSuite,
} from '@hg/testing';
import { v4 as uuid } from 'uuid';

import { TestEntity } from './test.entity';
import { TestMemoryRepository } from './test.memory.repository';
import { TestNotFoundException } from './test.not-found.exception';

@Describe()
export class BaseRepositoryTestSuite extends TestSuite {
	private readonly repository = new TestMemoryRepository();

	@Test()
	public async saveTest(): Promise<void> {
		const id = uuid();
		const entity = new TestEntity(id);

		await this.repository.save(entity);

		const savedEntity = await this.repository.getOrFail(id);

		expect(savedEntity).toBeInstanceOf(TestEntity);
		expect(savedEntity).not.toBe(entity);
		expect(savedEntity).toMatchObject(entity as any);
	}

	@Test()
	@ExpectError(TestNotFoundException)
	public async getOrFailNotFoundExceptionTest(): Promise<void> {
		const id = uuid();

		await this.repository.getOrFail(id);
	}

	@Test()
	public async getNotFoundWithoutExceptionTest(): Promise<void> {
		const id = uuid();

		await this.repository.get(id);
	}

	public async beforeEach(): Promise<void> {
		await this.repository.clear();
	}
}
