import { EntityNotFoundException } from '../exceptions';
import type { TestEntity } from './test.entity';

export class TestNotFoundException extends EntityNotFoundException {
	public constructor(id: Nullable<TestEntity['id']> = null) {
		super('Test', 0, id);
	}
}
