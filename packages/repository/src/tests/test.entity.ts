import { BaseEntity } from '@hg/entity';

export class TestEntity extends BaseEntity {
	public someField: Nullable<string> = null;

	public constructor(id: string) {
		super(id);
	}

	public setSomeField(value: Nullable<string>): void {
		this.someField = value;
	}
}
