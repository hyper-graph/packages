import { BaseException } from '../exceptions';

export class TestException extends BaseException {
	public constructor(message: string, code?: number) {
		super(message, code);
	}
}
