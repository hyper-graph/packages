import {
	Test,
	TestSuite,
	Describe,
	expect,
} from '@hg/testing';
import { BaseException } from '../exceptions';
import { TestException } from './test.exception';

@Describe()
export class SerializationTestSuite extends TestSuite {
	@Test()
	public async commonTest(): Promise<void> {
		const message = 'Test message';
		const exception = new TestException(message);

		expect(exception).toBeInstanceOf(Error);
		expect(exception).toBeInstanceOf(BaseException);
		expect(exception).toBeInstanceOf(TestException);
		expect(exception.message).toEqual(message);
		expect(exception.code).toBeNull();
		expect(exception.toJSON().name).toEqual(TestException.name);
		expect(exception.toJSON().stack).toBeInstanceOf(Array);
		expect(exception.toJSON().stack).not.toEqual([]);
	}

	@Test()
	public async syncStackTest(): Promise<void> {
		const { stack } = new TestException('Test message').toJSON();
		expect(stack[0]!.startsWith('at SerializationTestSuite.syncStackTest')).toBeTruthy();
	}

	@Test()
	public async isFromNextTickTest(): Promise<void> {
		return new Promise<void>(resolve => {
			process.nextTick(() => {
				const exception = new TestException('Test message');

				expect(exception.isFromNextTick()).toBeTruthy();
				resolve();
			});
		});
	}

	@Test()
	public async isFromImmediateTest(): Promise<void> {
		return new Promise<void>(resolve => {
			setImmediate(() => {
				const exception = new TestException('Test message');

				expect(exception.isFromImmediate()).toBeTruthy();
				resolve();
			});
		});
	}

	@Test()
	public async fromTimeoutTest(): Promise<void> {
		return new Promise<void>(resolve => {
			setTimeout(() => {
				const exception = new TestException('Test message');

				expect(exception.isFromTimer()).toBeTruthy();
				resolve();
			}, 0);
		});
	}

	@Test()
	public async fromIntervalTest(): Promise<void> {
		let interval: NodeJS.Timeout;
		return new Promise<void>(resolve => {
			interval = setInterval(() => {
				const exception = new TestException('Test message');

				expect(exception.isFromTimer()).toBeTruthy();
				clearInterval(interval);
				resolve();
			}, 0);
		});
	}
}
