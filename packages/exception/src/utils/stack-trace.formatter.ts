import os from 'os';
import { SOURCE_MAP_ENABLED } from '../consts';

export class StackTraceFormatter {
	public static format(stack: string, exceptionName: string): string[] {
		return StackTraceFormatter.getStackLines(stack, exceptionName);
	}

	protected static getStackLines(stack: string, exceptionName: string): string[] {
		let lines = stack.split(os.EOL)
			.splice(1)
			.filter(stackLine => !StackTraceFormatter.isInternalStackLine(stackLine))
			.map(line => line.trim());

		if (SOURCE_MAP_ENABLED) {
			lines = StackTraceFormatter.mergeSourceMappedLines(lines.slice(4));
		}
		const lineFromExceptionConstructor = lines.findIndex(line => line.startsWith(`at new ${exceptionName}`));

		if (lineFromExceptionConstructor > -1) {
			return lines.slice(lineFromExceptionConstructor + 1);
		}

		return lines;
	}

	protected static mergeSourceMappedLines(lines: string[]): string[] {
		const result: string[] = [];

		lines.forEach((line, index) => {
			if (index % 2 === 0) {
				result.push(line);
			} else {
				const lastLine = result[result.length - 1];
				const pathToRuntimeCode = StackTraceFormatter.getRuntimeCodePath(lastLine!);
				const pathToSourceCode = StackTraceFormatter.getSourceCodePath(line);

				result[result.length - 1] = lastLine!.replace(pathToRuntimeCode, pathToSourceCode);
			}
		});

		return result;
	}

	protected static getRuntimeCodePath(stackLine: string): string {
		const openBraceIndex = stackLine.lastIndexOf('(');
		const closeBraceIndex = stackLine.lastIndexOf(')');
		const codePathStartIndex = openBraceIndex + 1;
		const pathToCodeLength = closeBraceIndex - codePathStartIndex;

		return stackLine.substr(codePathStartIndex, pathToCodeLength);
	}

	protected static getSourceCodePath(stackLine: string): string {
		return stackLine.trim().replace('-> ', '');
	}

	protected static isInternalStackLine(line: string): boolean {
		return line.includes('internal/') && !line.includes('processTicksAndRejections');
	}
}
