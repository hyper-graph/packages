import * as os from 'os';
import { InspectOptionsStylized, inspect } from 'util';
import '@hg/common';

import { StackTraceFormatter } from '../utils';

export type JSONBaseException = {
	name: string;
	code: Nullable<number>;
	message: string;
	stack: string[];
};

export abstract class BaseException extends Error {
	public readonly code: Nullable<number> = null;
	public readonly stack: string;
	public readonly message: string;
	private serialized: Nullable<JSONBaseException> = null;

	protected constructor(message: string, code?: number) {
		super(message);
		this.message = message;
		Error.captureStackTrace(this);
		this.code = code ?? null;
	}

	public get name(): string {
		const { name } = Object.getPrototypeOf(this).constructor as Function;

		return name;
	}

	public isFromNextTick(): boolean {
		const lastLine = this.getLastStackLine();

		return lastLine.startsWith('at processTicksAndRejections');
	}

	public isFromImmediate(): boolean {
		const lastLine = this.getLastStackLine();

		return lastLine.startsWith('at Immediate._onImmediate');
	}

	public isFromTimer(): boolean {
		const lastLine = this.getLastStackLine();

		return lastLine.startsWith('at Timeout._onTimeout');
	}

	public toJSON(stackDepth: Nullable<number> = null): JSONBaseException {
		if (this.serialized === null) {
			const { name, message, stack, code } = this;

			this.serialized = {
				name,
				code,
				message,
				stack: StackTraceFormatter.format(stack, this.name),
			};
		}

		if (stackDepth === null) {
			return this.serialized;
		}

		return {
			...this.serialized,
			stack: this.serialized.stack.slice(0, stackDepth),
		};
	}

	public toString(stackDepth: Nullable<number> = null): string {
		const { name, code, message, stack } = this.toJSON(stackDepth);
		const formattedStack = stack.map(line => `${' '.repeat(4)}${line}`);

		const firstLine = `${name}${code === null ? '' : ` [code: ${code}]`}: ${message}`;

		const errorLines = [
			firstLine,
			...formattedStack,
		];

		return errorLines.join(os.EOL);
	}

	public [inspect.custom](depth: number | null, { stylize }: InspectOptionsStylized): string {
		return stylize(this.toString(depth), 'regexp');
	}

	private getLastStackLine(): string {
		const { stack } = this.toJSON();

		return stack[stack.length - 1] ?? '';
	}
}
