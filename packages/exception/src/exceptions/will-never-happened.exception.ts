import { BaseException } from './base.exception';

export class WillNeverHappenedException extends BaseException {
	public constructor(message: string) {
		super(message);
	}
}
