import expect from 'expect';
type Class = new (...args: any[]) => any;

export function ExpectError(error: Class) {
	return function(_target: unknown, _key: string, descriptor: PropertyDescriptor): PropertyDescriptor {
		return {
			async value(...args: unknown[]): Promise<void> {
				let isThrowError = false;

				try {
					if (typeof descriptor.value === 'function') {
						await descriptor.value.call(this, ...args);
					}
				} catch (err: unknown) {
					isThrowError = true;
					expect(err).toBeInstanceOf(error);
				} finally {
					expect(isThrowError).toBeTruthy();
				}
			},
		};
	};
}
