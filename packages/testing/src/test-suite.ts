import type {
	MethodName,
	Options,
} from './decorators';

export type TestSuiteConstructor = new () => TestSuite;
export abstract class TestSuite {
	public testsMap: Map<MethodName, Options> | null;

	public async setUp(): Promise<void> {}

	public async tearDown(): Promise<void> {}

	public async beforeEach(): Promise<void> {}

	public async afterEach(): Promise<void> {}
}
