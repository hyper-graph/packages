import dotenv from 'dotenv';

const { parsed, error } = dotenv.config();

if (error) {
	throw error;
}

const shellScriptLines = [];

for (const key in parsed) {
	const value = parsed[key].split('#')[0].trim();
	if (value.length) {
		shellScriptLines.push(`export ${key}="${value}"`);
	}
}

console.log(shellScriptLines.join('\n'));
