import {
	BaseInjectable,
	getAllProviders,
	getInstanceClass,
	getInstancePrototype,
} from '@hg-nest/common';
import { Inject } from '@nestjs/common';
import { ModulesContainer } from '@nestjs/core';

import type { HandlerNotFoundException } from './handler-not-found.exception';

export abstract class BaseBus extends BaseInjectable {
	@Inject()
	protected readonly container: ModulesContainer;

	protected abstract handlersMap: Map<Class, any> = new Map()

	protected abstract providerFilter: (instance: any) => boolean;
	protected abstract getHandlerEntity: (handler: any) => Class;

	protected async handleModuleInit(): Promise<void> {
		getAllProviders(this.container)
			// eslint-disable-next-line @typescript-eslint/no-unsafe-return
			.map(({ instance }) => instance)
			.filter(this.providerFilter)
			.forEach(handler => {
				const handlerEntity = this.getHandlerEntity(handler);

				this.handlersMap.set(handlerEntity, handler);
			});
	}

	// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
	protected getHandlers<HandlerType>(entity: any): HandlerType[] & [HandlerType] {
		const entityClasses = [...this.handlersMap.keys()]
			.filter(queryClass => entity instanceof queryClass);

		if (entityClasses.length === 0) {
			throw this.getHandlerNotFoundException(getInstanceClass(entity)!.name);
		}

		const correctOrder: Class[] = this.getEntityPrototypeOrder(entity)
			.slice(0, -2)
			.map(({ constructor: ctor }) => ctor as Class);

		const sortedClasses = correctOrder.filter(ctor => entityClasses.includes(ctor));

		// eslint-disable-next-line @typescript-eslint/no-unsafe-return
		return sortedClasses.map(entityClass => this.handlersMap.get(entityClass)!) as [HandlerType];
	}

	protected abstract getHandlerNotFoundException(name: string): HandlerNotFoundException;

	private getEntityPrototypeOrder(entity: any): Record<any, any>[] {
		const prototype = getInstancePrototype(entity);

		if (prototype === null) {
			return [];
		}

		const innerPrototypes = this.getEntityPrototypeOrder(prototype);

		// eslint-disable-next-line @typescript-eslint/no-unsafe-return
		return [prototype, ...innerPrototypes];
	}
}
