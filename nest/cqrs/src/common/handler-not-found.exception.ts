import { BaseException } from '@hg/exception';

export abstract class HandlerNotFoundException extends BaseException {
	public constructor(handlerType: string, entityName: string) {
		super(`Handler not found for ${handlerType} "${entityName}"`);
	}
}
