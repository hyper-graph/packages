import {
	BaseBus,
	HandlerNotFoundException,
} from '../common';
import type {
	BaseCommand,
	CommandResult,
} from './base.command';
import {
	BaseCommandHandler,
	getHandlerCommand,
	isCommandHandler,
} from './base.command-handler';
import { CommandHandlerNotFoundException } from './exceptions';

export class CommandBus extends BaseBus {
	protected providerFilter = isCommandHandler;
	protected getHandlerEntity = getHandlerCommand;

	protected readonly handlersMap: Map<Class<BaseCommand>, BaseCommandHandler<any>> = new Map();

	public async execute<Command extends BaseCommand>(command: Command): Promise<CommandResult<Command>> {
		const [handler] = this.getHandlers<BaseCommandHandler<Command>>(command);

		const result = await handler.execute(command);

		command.setResult(result);

		return result;
	}

	protected getHandlerNotFoundException(name: string): HandlerNotFoundException {
		return new CommandHandlerNotFoundException(name);
	}
}
