const COMMAND_RESULT_SET = Symbol('@hyper-graph/cqrs COMMAND_RESULT_SET'
	+ '');

export abstract class BaseCommand<ResultType = unknown> {
	private [COMMAND_RESULT_SET]: Nullable<ResultType> = null;

	public getResult(): Nullable<ResultType> {
		return this[COMMAND_RESULT_SET];
	}

	public setResult(result: ResultType): void {
		this[COMMAND_RESULT_SET] = result;
	}
}

export type CommandResult<Command extends BaseCommand> = Command extends BaseCommand<infer X> ? X : never;
