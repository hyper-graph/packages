import { HandlerNotFoundException } from '../../common';

export class CommandHandlerNotFoundException extends HandlerNotFoundException {
	public constructor(name: string) {
		super('command', name);
	}
}
