import {
	BaseInjectable,
	getInstanceClass,
} from '@hg-nest/common';
import { HANDLER_METADATA } from '../consts';
import type {
	BaseCommand,
	CommandResult,
} from './base.command';

export abstract class BaseCommandHandler<Command extends BaseCommand<any>> extends BaseInjectable {
	public abstract execute(command: Command): Promise<CommandResult<Command>>;
}

export function CommandHandler(command: Class<BaseCommand>): ClassDecorator {
	return function(target: Function): void {
		Reflect.defineMetadata(HANDLER_METADATA, command, target);
	};
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function isCommandHandler(instance: any): instance is BaseCommandHandler<any> {
	return instance instanceof BaseCommandHandler;
}

export function getHandlerCommand(handler: BaseCommandHandler<any>): Class<BaseCommand> {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return Reflect.getMetadata(HANDLER_METADATA, getInstanceClass(handler)!);
}
