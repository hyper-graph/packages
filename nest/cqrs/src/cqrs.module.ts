import {
	Global,
	Module,
} from '@nestjs/common';

import { CommandBus } from './command';
import { EventBus } from './event';
import { QueryBus } from './query';

@Global()
@Module({
	providers: [CommandBus, QueryBus, EventBus],
	exports: [CommandBus, QueryBus, EventBus],
})
export class CqrsModule {}
