import {
	Describe,
	expect,
	ExpectError,
	Test,
	TestSuite,
} from '@hg/testing';
import { Test as TestFactory } from '@nestjs/testing';
import { CqrsModule } from '../../cqrs.module';

import {
	EventBus,
	EventHandlerNotFoundException,
} from '../../event';

import { GroupEvent } from './group.event';
import { GroupEventHandler } from './group.event-handler';
import { TargetEvent } from './target.event';
import { TargetEventHandler } from './target.event-handler';
import { WithoutHandlerEvent } from './without-handler.event';

@Describe()
export class EventBusTestSuite extends TestSuite {
	private eventBus: EventBus;

	@Test()
	public async groupEventExecuteTest(): Promise<void> {
		const randomNumber = Math.random();

		await this.eventBus.publish(new GroupEvent(randomNumber));

		// 2 assertion in the GroupEventHandler
		expect.assertions(2);
	}

	@Test()
	public async targetEventExecuteTest(): Promise<void> {
		const randomNumber = Math.random();

		await this.eventBus.publish(new TargetEvent(randomNumber));

		/*
		 * 2 assertion in the GroupEventHandler
		 * 3 assertion in the TargetEventHandler
		 */
		expect.assertions(5);
	}

	@Test()
	@ExpectError(EventHandlerNotFoundException)
	public async handlerNotFoundException(): Promise<void> {
		await this.eventBus.publish(new WithoutHandlerEvent());
	}

	public async setUp(): Promise<void> {
		const module = await TestFactory.createTestingModule({
			imports: [CqrsModule],
			providers: [GroupEventHandler, TargetEventHandler],
		})
			.compile();

		await module.init();

		this.eventBus = module.get(EventBus);
	}
}
