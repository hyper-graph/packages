import { expect } from '@hg/testing';

import {
	BaseEventHandler,
	EventHandler,
} from '../../event';
import { GroupEvent } from './group.event';
import { TargetEvent } from './target.event';

@EventHandler(TargetEvent)
export class TargetEventHandler extends BaseEventHandler<TargetEvent> {
	public async handle(event: TargetEvent): Promise<void> {
		expect(event).toBeInstanceOf(TargetEvent);
		expect(event).toBeInstanceOf(GroupEvent);
		expect(event.data).toBeDefined();
	}
}
