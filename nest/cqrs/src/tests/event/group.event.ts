import { BaseEvent } from '../../event';

export class GroupEvent extends BaseEvent {
	public readonly data: number;

	public constructor(data: number) {
		super();
		this.data = data;
	}
}
