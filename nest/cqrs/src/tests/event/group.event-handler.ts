import { expect } from '@hg/testing';

import {
	BaseEventHandler,
	EventHandler,
} from '../../event';
import { GroupEvent } from './group.event';

@EventHandler(GroupEvent)
export class GroupEventHandler extends BaseEventHandler<GroupEvent> {
	public async handle(event: GroupEvent): Promise<void> {
		expect(event).toBeInstanceOf(GroupEvent);
		expect(event.data).toBeDefined();
	}
}
