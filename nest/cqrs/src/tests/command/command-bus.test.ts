import {
	Describe,
	expect,
	ExpectError,
	Test,
	TestSuite,
} from '@hg/testing';
import { Test as TestFactory } from '@nestjs/testing';

import {
	CommandBus,
	CommandHandlerNotFoundException,
} from '../../command';
import { CqrsModule } from '../../cqrs.module';

import { TestCommand } from './test.command';
import { TestCommandHandler } from './test.command-handler';
import { WithoutHandlerCommand } from './without-handler.command';

@Describe()
export class CommandBusTestSuite extends TestSuite {
	private commandBus: CommandBus;

	@Test()
	public async executeCommandTest(): Promise<void> {
		const randomNumber = Math.random();
		const command = new TestCommand(randomNumber);

		expect(command.getResult()).toBeNull();

		const result = await this.commandBus.execute(command);

		expect(command.getResult()).not.toBeNull();
		expect(command.getResult()).toBe(randomNumber);
		expect(result).toBe(randomNumber);

		// The 5-th assertion in the TestCommandHandler
		expect.assertions(5);
	}

	@Test()
	@ExpectError(CommandHandlerNotFoundException)
	public async handlerNotFoundException(): Promise<void> {
		await this.commandBus.execute(new WithoutHandlerCommand());
	}

	public async setUp(): Promise<void> {
		const module = await TestFactory.createTestingModule({
			imports: [CqrsModule],
			providers: [TestCommandHandler],
		})
			.compile();

		await module.init();

		this.commandBus = module.get(CommandBus);
	}
}
