import { expect } from '@hg/testing';

import {
	BaseCommandHandler,
	CommandHandler,
} from '../../command';
import { TestCommand } from './test.command';

@CommandHandler(TestCommand)
export class TestCommandHandler extends BaseCommandHandler<TestCommand> {
	public async execute(command: TestCommand): Promise<number> {
		expect(command).toBeInstanceOf(TestCommand);

		return command.data;
	}
}
