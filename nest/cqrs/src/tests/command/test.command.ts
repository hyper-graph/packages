import { BaseCommand } from '../../command';

export class TestCommand extends BaseCommand<number> {
	public readonly data: number;

	public constructor(data: number) {
		super();
		this.data = data;
	}
}
