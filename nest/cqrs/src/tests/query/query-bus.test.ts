import {
	Describe,
	expect,
	ExpectError,
	Test,
	TestSuite,
} from '@hg/testing';
import { Test as TestFactory } from '@nestjs/testing';
import { CqrsModule } from '../../cqrs.module';

import {
	QueryBus,
	QueryHandlerNotFoundException,
} from '../../query';

import { TestQuery } from './test.query';
import { TestQueryHandler } from './test.query-handler';
import { WithoutHandlerQuery } from './without-handler.query';

@Describe()
export class QueryBusTestSuite extends TestSuite {
	private queryBus: QueryBus;

	@Test()
	public async executeQueryTest(): Promise<void> {
		const randomNumber = Math.random();
		const query = new TestQuery(randomNumber);

		expect(query.getResult()).toBeNull();

		const result = await this.queryBus.execute(query);

		expect(query.getResult()).not.toBeNull();
		expect(query.getResult()).toBe(randomNumber);
		expect(result).toBe(randomNumber);

		// The 5-th assertion in the TestQueryHandler
		expect.assertions(5);
	}

	@Test()
	@ExpectError(QueryHandlerNotFoundException)
	public async handlerNotFoundException(): Promise<void> {
		await this.queryBus.execute(new WithoutHandlerQuery());
	}

	public async setUp(): Promise<void> {
		const module = await TestFactory.createTestingModule({
			imports: [CqrsModule],
			providers: [TestQueryHandler],
		})
			.compile();

		await module.init();

		this.queryBus = module.get(QueryBus);
	}
}
