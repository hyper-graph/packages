import { expect } from '@hg/testing';

import {
	BaseQueryHandler,
	QueryHandler,
} from '../../query';
import { TestQuery } from './test.query';

@QueryHandler(TestQuery)
export class TestQueryHandler extends BaseQueryHandler<TestQuery> {
	public async execute(query: TestQuery): Promise<number> {
		expect(query).toBeInstanceOf(TestQuery);

		return query.data;
	}
}
