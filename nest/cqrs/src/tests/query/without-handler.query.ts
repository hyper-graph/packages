import { BaseQuery } from '../../query';

export class WithoutHandlerQuery extends BaseQuery<void> {}
