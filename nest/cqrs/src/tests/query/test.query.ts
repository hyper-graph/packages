import { BaseQuery } from '../../query';

export class TestQuery extends BaseQuery<number> {
	public readonly data: number;

	public constructor(data: number) {
		super();
		this.data = data;
	}
}
