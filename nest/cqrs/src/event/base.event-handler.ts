import {
	BaseInjectable,
	getInstanceClass,
} from '@hg-nest/common';

import { HANDLER_METADATA } from '../consts';
import type { BaseEvent } from './base.event';

export abstract class BaseEventHandler<Event extends BaseEvent> extends BaseInjectable {
	public abstract handle(event: Event): Promise<void>;
}

export function EventHandler(event: Class<BaseEvent>): ClassDecorator {
	return function(target: Function): void {
		Reflect.defineMetadata(HANDLER_METADATA, event, target);
	};
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function isEventHandler(instance: any): instance is BaseEventHandler<any> {
	return instance instanceof BaseEventHandler;
}

export function getHandlerEvent(handler: BaseEventHandler<any>): Class<BaseEvent> {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return Reflect.getMetadata(HANDLER_METADATA, getInstanceClass(handler)!);
}
