const EVENT_RESULT_SET = Symbol('@hyper-graph/cqrs EVENT_RESULT_SET');

export abstract class BaseEvent {
	// @ts-expect-error
	private [EVENT_RESULT_SET] = null;
}
