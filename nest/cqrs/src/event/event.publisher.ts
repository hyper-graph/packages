import type { BaseEvent } from './base.event';

export abstract class EventPublisher {
	public abstract publish(event: BaseEvent): Promise<void>;
}
