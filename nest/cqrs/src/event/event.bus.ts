import {
	Inject,
	Optional,
} from '@nestjs/common';
import {
	BaseBus,
	HandlerNotFoundException,
} from '../common';
import type { BaseEvent } from './base.event';
import {
	BaseEventHandler,
	getHandlerEvent,
	isEventHandler,
} from './base.event-handler';
import { EventPublisher } from './event.publisher';
import { EventHandlerNotFoundException } from './exceptions';

export class EventBus extends BaseBus {
	@Inject()
	@Optional()
	protected readonly publisher?: EventPublisher;

	protected providerFilter = isEventHandler;
	protected getHandlerEntity = getHandlerEvent;

	protected readonly handlersMap: Map<Class<BaseEvent>, BaseEventHandler<any>> = new Map();

	public async publish(event: BaseEvent): Promise<void> {
		const handlers = this.getHandlers<BaseEventHandler<BaseEvent>>(event);

		const localHandlers = Promise.all(handlers.map(async handler => handler.handle(event)));
		const remoteHandlers = this.publisher
			? this.publisher.publish(event)
			: Promise.resolve();

		await Promise.all([localHandlers, remoteHandlers]);
	}

	protected getHandlerNotFoundException(name: string): HandlerNotFoundException {
		return new EventHandlerNotFoundException(name);
	}
}
