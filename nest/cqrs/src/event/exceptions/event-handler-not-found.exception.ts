import { HandlerNotFoundException } from '../../common';

export class EventHandlerNotFoundException extends HandlerNotFoundException {
	public constructor(name: string) {
		super('event', name);
	}
}
