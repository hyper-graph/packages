import {
	BaseInjectable,
	getInstanceClass,
} from '@hg-nest/common';
import { HANDLER_METADATA } from '../consts';
import type {
	BaseQuery,
	QueryResult,
} from './base.query';

export abstract class BaseQueryHandler<Query extends BaseQuery<any>> extends BaseInjectable {
	public abstract execute(query: Query): Promise<QueryResult<Query>>;
}

export function QueryHandler(query: Class<BaseQuery>): ClassDecorator {
	return function(target: Function): void {
		Reflect.defineMetadata(HANDLER_METADATA, query, target);
	};
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function isQueryHandler(instance: any): instance is BaseQueryHandler<any> {
	return instance instanceof BaseQueryHandler;
}

export function getHandlerQuery(handler: BaseQueryHandler<any>): Class<BaseQuery> {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return Reflect.getMetadata(HANDLER_METADATA, getInstanceClass(handler)!);
}
