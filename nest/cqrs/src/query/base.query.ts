const QUERY_RESULT_SET = Symbol('@hyper-graph/cqrs QUERY_RESULT_SET');

export abstract class BaseQuery<ResultType = unknown> {
	private [QUERY_RESULT_SET]: Nullable<ResultType> = null;

	public getResult(): Nullable<ResultType> {
		return this[QUERY_RESULT_SET];
	}

	public setResult(result: ResultType): void {
		this[QUERY_RESULT_SET] = result;
	}
}

export type QueryResult<Query extends BaseQuery> = Query extends BaseQuery<infer X> ? X : never;
