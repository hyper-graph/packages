import { HandlerNotFoundException } from '../../common';

export class QueryHandlerNotFoundException extends HandlerNotFoundException {
	public constructor(name: string) {
		super('query', name);
	}
}
