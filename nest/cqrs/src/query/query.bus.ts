import {
	BaseBus,
	HandlerNotFoundException,
} from '../common';
import type {
	BaseQuery,
	QueryResult,
} from './base.query';
import {
	BaseQueryHandler,
	getHandlerQuery,
	isQueryHandler,
} from './base.query-handler';
import { QueryHandlerNotFoundException } from './exceptions';

export class QueryBus extends BaseBus {
	protected providerFilter = isQueryHandler;
	protected getHandlerEntity = getHandlerQuery;
	protected readonly handlersMap: Map<Class<BaseQuery>, BaseQueryHandler<any>> = new Map();

	public async execute<Query extends BaseQuery>(query: Query): Promise<QueryResult<Query>> {
		const [handler] = this.getHandlers<BaseQueryHandler<Query>>(query);

		const result = await handler.execute(query);

		query.setResult(result);

		return result;
	}

	protected getHandlerNotFoundException(name: string): HandlerNotFoundException {
		return new QueryHandlerNotFoundException(name);
	}
}
