import type { BaseFormat } from '../formatter';
import type { BaseProtocol } from '../protocol';

export type Pattern = RegExp | string;

export type CommonHandlerOptions = {
	method: Pattern;
	request?: true;
	event?: true;
};

export type HandlerOptions = CommonHandlerOptions;

export type AdapterOptions = {
	namespaceDelimiter?: string;
	formatter?: BaseFormat;
	protocol?: BaseProtocol;
};

export type GatewayOptions = {
	namespaces?: string[];
	path?: string | null;
};
