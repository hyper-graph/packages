import {
	ArgumentsHost,
	Catch,
} from '@nestjs/common';
import { BaseWsExceptionFilter } from '@nestjs/websockets';

@Catch()
export class CommonExceptionFilter extends BaseWsExceptionFilter {
	public catch(exception: unknown, _host: ArgumentsHost): void {
		throw exception;
	}
}
