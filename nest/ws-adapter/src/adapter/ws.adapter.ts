import {
	AbstractWsAdapter,
	MessageMappingProperties,
} from '@nestjs/websockets';
import {
	INestApplication,
	Logger,
} from '@nestjs/common';
import {
	InvalidFormatException,
	JsonFormat,
	UnsupportedFormatException,
} from '../formatter';

import {
	IncomingMessage,
	OutgoingHttpHeaders,
	Server,
	createServer,
} from 'http';
import type { Observable } from 'rxjs';
import type { Socket } from 'net';
import Url from 'url';

import Websocket, { Server as WebSocketServer } from 'ws';
import { format } from 'util';
import { v4 as uuid } from 'uuid';

import { HandlerHelper } from '../helper';
import { JsonRpcProtocol } from '../protocol';
import {
	FrameData,
	getFrameType,
} from '../utils';
import type {
	AdapterOptions,
	GatewayOptions,
} from './options';

const VERIFY_CLIENT_RESULT = Symbol('Verify client result');

type TransformCallback = (data: any) => Observable<any>;

export type WsAdapterOptions = GatewayOptions & {
	server?: Websocket.Server;
};

export type VerifyClient = (info: VerifyClientInfo) => Promise<VerifyClientResult>;
export type ServerInfo<DataType = any> = {
	port: number;
	verifyClient: VerifyClient | null;
	options: Required<GatewayOptions>;
	data: DataType;
	clients: Map<string, ClientInfo>;
};
type BaseClientInfo = {
	client: Websocket;
	gatewayOptions: Required<GatewayOptions>;
	request: IncomingMessage;
};
export type ClientInfo<DataType = any> = BaseClientInfo & {
	sendEvent(event: string, data: any): void;
	data: DataType;
	id: string;
};
export type VerifyClientInfo = {
	origin: string;
	secure: boolean;
	req: IncomingMessage;
};
export type VerifyClientResult = {
	headers: OutgoingHttpHeaders | null;
};

type VerifyClientCallback = (res: boolean, code?: number, message?: string, headers?: OutgoingHttpHeaders) => void;

type RealServerInfo = {
	server: Server;
	wsServer: Websocket.Server;
	handlers: Set<ServerInfo>;
	upgradeHandlerSet: boolean;
};
type RealClientInfo = {
	client: Websocket;
	optionsSet: Set<GatewayOptions>;
	helper: HandlerHelper;
	messageHandlerSet: boolean;
};
type ServerMap = Map<number, RealServerInfo>;
type ClientMap = Map<Websocket, RealClientInfo>;

const enum WebsocketCloseCode {
	SUCCESS = 1000,
	PARTICIPANT_STOP = 1001,
	PROTOCOL_ERROR = 1002,
	UNSUPPORTED_FRAME = 1003,
	INVALID_DATA = 1007,
	MESSAGE_TOO_BIG = 1009,
}

export class WebsocketAdapter extends AbstractWsAdapter<any, any, WsAdapterOptions> {
	declare protected httpServer: Server;

	private readonly logger = new Logger(WebsocketAdapter.name);

	private readonly options: Required<AdapterOptions>;

	private readonly serverMap: ServerMap = new Map();

	private readonly clientsMap: ClientMap = new Map();

	public constructor(app: INestApplication, options: AdapterOptions = {}) {
		super(app);

		this.options = {
			formatter: options.formatter || new JsonFormat(),
			namespaceDelimiter: options.namespaceDelimiter || '/',
			protocol: options.protocol || new JsonRpcProtocol(),
		};
	}

	public create(
		port: number,
		options: WsAdapterOptions,
	): ServerInfo {
		this.initServer(port);

		// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
		const realServerInfo = this.serverMap.get(port)!;

		const serverInfo: ServerInfo = {
			port,
			clients: new Map(),
			verifyClient: null,
			options: {
				namespaces: options.namespaces || [],
				path: options.path || null,
			},
			data: null,
		};

		realServerInfo.handlers.add(serverInfo);
		this.serverMap.set(port, realServerInfo);

		return serverInfo;
	}

	// eslint-disable-next-line max-lines-per-function
	public bindClientConnect(serverInfo: ServerInfo, callback: (connection: ClientInfo) => void): void {
		const { port, options } = serverInfo;
		// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
		const realServerInfo = this.serverMap.get(port)!;

		realServerInfo.wsServer.on('headers', (headers: string[], request: any) => {
			const { headers: additionalHeaders } = request[VERIFY_CLIENT_RESULT] as VerifyClientResult;
			if (additionalHeaders) {
				Object.keys(additionalHeaders)
					.forEach(headerName => {
						const headerValue = additionalHeaders[headerName]!;
						if (Array.isArray(headerValue)) {
							headerValue.forEach(value => headers.push(`${headerName}: ${value}`));
						} else {
							headers.push(`${headerName}: ${headerValue}`);
						}
					});
			}
		});
		realServerInfo.wsServer.on('connection', (client, request) => {
			let realClientInfo = this.clientsMap.get(client);

			if (!realClientInfo) {
				realClientInfo = {
					client,
					optionsSet: new Set(),
					helper: new HandlerHelper(this.options),
					messageHandlerSet: false,
				};
				this.clientsMap.set(client, realClientInfo);
			}

			const id = uuid();

			realClientInfo.optionsSet.add(options);
			const baseClientInfo: BaseClientInfo = {
				request,
				client,
				gatewayOptions: options,
			};
			const clientInfo: ClientInfo = {
				...baseClientInfo,
				data: null,
				sendEvent: async(event: string, data: any) => this.sendEvent(baseClientInfo, event, data),
				id,
			};
			serverInfo.clients.set(id, clientInfo);

			callback(clientInfo);
		});

		if (!realServerInfo.upgradeHandlerSet) {
			// eslint-disable-next-line @typescript-eslint/no-misused-promises
			realServerInfo.server.on('upgrade', async(
				request: IncomingMessage,
				socket: Socket,
				head: Buffer,
			) => this.handleUpgrade(realServerInfo, request, socket, head));

			realServerInfo.upgradeHandlerSet = true;
			this.serverMap.set(port, realServerInfo);
		}
	}

	public bindMessageHandlers(
		clientInfo: ClientInfo,
		handlers: MessageMappingProperties[],
		transform: TransformCallback,
	): void {
		const { client, gatewayOptions } = clientInfo;
		// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
		const realClientInfo = this.clientsMap.get(client)!;
		const formattedOptions = format(gatewayOptions);

		this.logger.debug(`bindMessageHandlers called. handlersCount: ${
			handlers.length
		}. gatewayOptions: ${formattedOptions}`);

		realClientInfo.helper.addGateway(handlers, gatewayOptions);

		if (realClientInfo.messageHandlerSet) {
			return;
		}

		// eslint-disable-next-line @typescript-eslint/no-misused-promises
		client.on('message', async data => this.handleMessage(realClientInfo, data as FrameData, transform));
	}

	public bindClientDisconnect(clientInfo: ClientInfo, callback: Function): void {
		// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
		const realClientInfo = this.clientsMap.get(clientInfo.client)!;

		realClientInfo.client.once('close', () => {
			callback();
			this.logger.debug('Client disconnected from server');
			clientInfo.client.removeAllListeners();
			realClientInfo.optionsSet.delete(clientInfo.gatewayOptions);
			this.serverMap.values();

			const clientServer = this.findClientServer(clientInfo);

			clientServer?.clients.delete(clientInfo.id);

			if (realClientInfo.optionsSet.size === 0) {
				this.logger.debug('Client removed from all servers');
				this.clientsMap.delete(clientInfo.client);
			}
		});
	}

	public async close(serverInfo: ServerInfo): Promise<void> {
		const { port } = serverInfo;
		// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
		const realServerInfo = this.serverMap.get(port)!;

		const realServerWillClosed = realServerInfo.handlers.size === 0;

		if (realServerWillClosed) {
			serverInfo.clients.forEach(({ client }) => client.removeAllListeners());
			realServerInfo.wsServer.clients.forEach(client => {
				client.removeAllListeners();
				client.close(WebsocketCloseCode.PARTICIPANT_STOP);
			});
		}

		realServerInfo.handlers.delete(serverInfo);

		if (realServerWillClosed) {
			realServerInfo.server.close();
			this.serverMap.delete(port);
		}
	}

	private initServer(port: number): void {
		if (this.serverMap.has(port)) {
			return;
		}

		let server: Server;

		if (port === 0) {
			server = this.httpServer;
		} else {
			server = createServer();
			server.listen(port);
		}

		this.serverMap.set(port, {
			server,
			wsServer: new WebSocketServer({ noServer: true }),
			handlers: new Set(),
			upgradeHandlerSet: false,
		});
	}

	private async sendEvent(clientInfo: BaseClientInfo, event: string, data: any): Promise<void> {
		const preparedData = await this.options.protocol.formatEvent(event, data);

		const formattedData = await this.options.formatter.format(preparedData);

		clientInfo.client.send(formattedData);
	}

	// eslint-disable-next-line max-params
	private async handleUpgrade(
		realServerInfo: RealServerInfo,
		request: IncomingMessage,
		socket: Socket,
		head: Buffer,
	): Promise<void> {
		const handlers = [...realServerInfo.handlers.values()]
			.filter(({ options }) => WebsocketAdapter.isPathAllow(options.path, request.url || null));

		realServerInfo.wsServer.options.verifyClient = this.verifyClientWrapper.bind(this, handlers);
		realServerInfo.wsServer.handleUpgrade(request, socket, head, client => {
			this.logger.debug('Upgrade handled successfully');
			realServerInfo.wsServer.emit('connection', client, request);
		});
	}

	private async handleMessage(
		clientInfo: RealClientInfo,
		frameData: FrameData,
		transform: TransformCallback,
	): Promise<void> {
		const frameType = getFrameType(frameData);

		this.logger.debug(`Receive frame. Frame type: ${frameType}`);

		let data: any;
		const { formatter, protocol } = this.options;

		try {
			data = await formatter.parse(frameData);
		} catch (error: unknown) {
			this.handleFormatterError(error, clientInfo.client);
			return;
		}

		this.logger.debug(`Input data: ${format(data)}`);

		const result = await protocol.handle(data, clientInfo.helper, transform);

		this.logger.debug(`Output data: ${format(result)}`);

		if (typeof result !== 'undefined') {
			clientInfo.client.send(await formatter.format(result));
		}
	}

	private async verifyClientWrapper(
		handlers: ServerInfo[],
		info: VerifyClientInfo,
		callback: VerifyClientCallback,
	): Promise<void> {
		try {
			this.logger.debug('Verify client start');
			const result = await this.verifyClient(handlers, info);

			(info.req as any)[VERIFY_CLIENT_RESULT] = result;

			this.logger.debug('Client verified');

			// eslint-disable-next-line no-undefined
			callback(true, undefined, undefined, result.headers || undefined);
		} catch (error: unknown) {
			this.logger.debug('Client rejected');
			// eslint-disable-next-line no-undefined
			callback(false, WebsocketCloseCode.INVALID_DATA, (error as any)?.message, undefined);
		}
	}

	private async verifyClient(handlers: ServerInfo[], info: VerifyClientInfo): Promise<VerifyClientResult> {
		this.logger.debug('Verify client');
		const verifyClientHandlers = handlers.map(({ verifyClient }) => verifyClient)
			.filter((handler): handler is VerifyClient => handler !== null);
		const results = await Promise.all(verifyClientHandlers.map(async handler => handler(info)));

		return this.mergeVerifyResults(results);
	}

	private mergeVerifyResults(results: VerifyClientResult[]): VerifyClientResult {
		const headers: OutgoingHttpHeaders = this.mergeHeaders(results.map(({ headers }) => headers));

		return { headers };
	}

	private mergeHeaders(headersList: (OutgoingHttpHeaders | null)[]): OutgoingHttpHeaders {
		return headersList.filter((headers): headers is OutgoingHttpHeaders => headers !== null)
			.reduce((result: OutgoingHttpHeaders, headers: OutgoingHttpHeaders) => {
				for (const headerName in headers) {
					result[headerName] = headers[headerName];
				}

				return result;
			}, {});
	}

	private handleFormatterError(error: any, client: Websocket): void {
		const { protocol, formatter } = this.options;

		if (error instanceof UnsupportedFormatException) {
			client.close(WebsocketCloseCode.UNSUPPORTED_FRAME);
		} else if (error instanceof InvalidFormatException) {
			if (protocol.closeOnFormatterError()) {
				client.close(WebsocketCloseCode.INVALID_DATA);
			} else {
				const formattedError = protocol.formatFormatterError(error.parseError);

				client.send(formatter.format(formattedError));
			}
		} else {
			this.logger.error('Formatter thrown unknown error');
			this.logger.error(error);
		}
	}

	private findClientServer(client: ClientInfo): ServerInfo | null {
		const server = [...this.serverMap.values()]
			.flatMap(realServer => [...realServer.handlers.values()])
			.find(server => server.clients.has(client.id));

		if (!server) {
			this.logger.warn(`Server info for client with id ${client.id} not found`);
		}

		return server || null;
	}

	private static isPathAllow(allowPath: string | null, requestUrl: string | null): boolean {
		if (allowPath === null || requestUrl === null) {
			return true;
		}

		const { path: requestPath } = Url.parse(requestUrl);

		if (!requestPath) {
			return true;
		}

		return requestPath.startsWith(allowPath);
	}
}
