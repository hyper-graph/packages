import {
	Describe,
	Test,
	TestSuite,
	expect,
} from '@hg/testing';
import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import {
	FastifyAdapter,
	NestFastifyApplication,
} from '@nestjs/platform-fastify';
import random from 'random';
import Websocket from 'ws';
import type { JsonRpcResponse } from '../protocol';

import { TestModule } from '../test-utils';

import { WebsocketAdapter } from './ws.adapter';

@Describe()
export class AdapterTestSuite extends TestSuite {
	private logger: Logger;

	private port: number;

	private defaultClient: Websocket;

	private application: NestFastifyApplication;

	@Test()
	public async echoTest(): Promise<void> {
		const params = { message: 'someData' };
		const method = 'echo';

		const response: any = await this.sendRequest(method, params);

		expect(response.error).toBeUndefined();
		expect(response.result).toEqual(params);
	}

	@Test()
	public async multipleHandlerMerge(): Promise<void> {
		const method = 'multiple';

		const response: any = await this.sendRequest(method);

		expect(response.error).toBeUndefined();
		expect(typeof response.result).toBe('object');
		expect(response.result).toEqual({
			data1: 1,
			data2: 2,
			data3: 3,
		});
	}

	@Test()
	public async errorHandling(): Promise<void> {
		const method = 'error';
		const params = { someData: 'data' };

		const response: any = await this.sendRequest(method, params);

		expect(typeof response.error).toBe('object');
		expect(response.result).toBeUndefined();
		expect(response.error.code).toBe(1);
		expect(typeof response.error.message).toBe('string');
		expect(response.error.data).toEqual(params);
	}

	@Test()
	public async pushbackMessage(): Promise<void> {
		const params = { message: 'someData' };
		const method = 'pushback';

		await this.sendRequest(method, params);

		const data: any = await this.waitPromise();

		expect(data.method).toEqual('another');
		expect(data.params).toEqual({ foo: 'bar' });
	}

	public async setUp(): Promise<void> {
		this.initApplicationPort();
		await this.initApplication();
		await this.initClients();
	}

	public async tearDown(): Promise<void> {
		this.defaultClient.close();
		await this.application.close();
	}

	private initApplicationPort(): void {
		// eslint-disable-next-line no-magic-numbers
		this.port = random.int(30000, 65000);
	}

	private async initApplication(): Promise<void> {
		this.application = await NestFactory.create<NestFastifyApplication>(
			TestModule,
			new FastifyAdapter(),
			{ logger: false },
		);

		this.application.useWebSocketAdapter(new WebsocketAdapter(this.application));

		await this.application.init();
		await this.application.listenAsync(this.port);
		this.logger = new Logger(AdapterTestSuite.name);
		this.logger.debug(`Port: ${this.port}`);
	}

	private async initClients(): Promise<void> {
		const defaultClientUrl = `ws://localhost:${this.port}`;

		this.defaultClient = new Websocket(defaultClientUrl);

		return new Promise<void>(resolve => {
			this.defaultClient.once('open', () => resolve());
		});
	}

	private async sendRequest(method: string, params?: any): Promise<JsonRpcResponse> {
		const message = {
			id: 1,
			jsonrpc: '2.0',
			method,
			params,
		};

		this.defaultClient.send(JSON.stringify(message));

		const response: any = await this.waitPromise();

		expect(response.jsonrpc).toBe(message.jsonrpc);
		expect(response.id).toBe(message.id);

		return response as JsonRpcResponse;
	}

	private readonly waitPromise = async(): Promise<unknown> => new Promise((resolve, reject) => {
		const timeout = setTimeout(() => {
			this.defaultClient.removeAllListeners('message');
			reject(new Error('TimeoutError'));
		}, 1000);

		this.defaultClient.once('message', data => {
			resolve(JSON.parse(data as string));
			clearTimeout(timeout);
		});
	});
}
