import { BaseException } from '@hg/exception';

export class HandlerNotFoundError extends BaseException {
	public constructor() {
		super('Handler not found', -32601);
	}
}
