import { UseFilters } from '@nestjs/common';
import {
	ConnectedSocket,
	MessageBody,
	SubscribeMessage,
	WebSocketGateway,
} from '@nestjs/websockets';
import {
	ClientInfo,
	CommonExceptionFilter,
} from '../adapter';
import { TextException } from './text.exception';

@WebSocketGateway()
export class DefaultGateway {
	@SubscribeMessage('echo')
	public echo(@MessageBody() data: unknown): unknown {
		return data;
	}

	@UseFilters(new CommonExceptionFilter())
	@SubscribeMessage('error')
	public error(@MessageBody() data: unknown): any {
		throw new TextException(data);
	}

	@SubscribeMessage('multiple')
	public multiple1(@MessageBody() _data: unknown): Record<string, number> {
		return { data1: 1 };
	}

	@SubscribeMessage('multiple')
	public multiple2(@MessageBody() _data: unknown): Record<string, number> {
		return { data2: 2 };
	}

	@SubscribeMessage('multiple')
	public multiple3(@MessageBody() _data: unknown): Record<string, number> {
		return { data3: 3 };
	}

	@SubscribeMessage('multiple')
	public multiple4(@MessageBody() _data: unknown): null {
		return null;
	}

	@SubscribeMessage('multiple')
	public multiple5(@MessageBody() _data: unknown): boolean {
		return true;
	}

	@SubscribeMessage('pushback')
	public pushBack(@ConnectedSocket() client: ClientInfo): void {
		setTimeout(() => client.sendEvent('another', { foo: 'bar' }), 10);
	}
}
