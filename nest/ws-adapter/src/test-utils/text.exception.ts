import {
	BaseException,
	JSONBaseException,
} from '@hg/exception';

export class TextException extends BaseException {
	public data: any;

	// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
	public constructor(data: any) {
		super('Error message', 1);

		this.data = data;
	}

	public toJSON(stackDepth?: Nullable<number>): JSONBaseException & { data: any } {
		const baseJson = super.toJSON(stackDepth);

		return {
			...baseJson,
			data: this.data,
		};
	}
}
