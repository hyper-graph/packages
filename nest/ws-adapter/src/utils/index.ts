export enum FrameType {
	TEXT = 'text',
	BINARY = 'binary',
}

export type FrameData = Buffer | string;

export function getFrameType(data: FrameData): FrameType {
	if (typeof data === 'string') {
		return FrameType.TEXT;
	}

	return FrameType.BINARY;
}
