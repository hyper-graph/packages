import type { Observable } from 'rxjs';
import { HandlerNotFoundError } from '../errors';
import type { HandlerHelper } from '../helper';

export abstract class BaseProtocol {
	public closeOnFormatterError(): boolean {
		return true;
	}

	public formatFormatterError(error: unknown): unknown {
		return error;
	}

	public abstract handle(
		data: unknown,
		helper: HandlerHelper,
		transform: (data: any) => Observable<any>
	): Promise<unknown>;

	public abstract formatEvent(event: string, data: unknown): Promise<any>;

	// eslint-disable-next-line max-params
	protected async call(
		helper: HandlerHelper,
		pattern: string,
		data: unknown,
		transform: (data: any) => Observable<any>,
	): Promise<any> {
		const handlers = helper.getPatternHandlers(pattern);

		if (handlers.length === 0) {
			throw new HandlerNotFoundError();
		}

		// eslint-disable-next-line @typescript-eslint/no-unsafe-return
		const results: unknown[] = await Promise.all(handlers.map(({ callback }) => callback(data)));
		const transformedResults = await Promise.all(results.map(async result => transform(result).toPromise()));

		return this.mergeResults(transformedResults);
	}

	private mergeResults(results: unknown[]): unknown {
		return results.reduce((summary: any, result: any) => {
			if (typeof result !== 'object' || result === null) {
				return summary;
			}

			if (summary === null) {
				return result;
			}

			return {
				...summary,
				...result,
			};
		}, null);
	}
}
