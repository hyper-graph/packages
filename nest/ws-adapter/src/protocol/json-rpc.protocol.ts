import { BaseException } from '@hg/exception';
import { WsException } from '@nestjs/websockets';
import type { HandlerHelper } from '../helper';
import { BaseProtocol } from './base.protocol';

type JsonRpcId = number | string | null;

type JsonRpcCommon = {
	jsonrpc: '2.0';
};

type JsonRpcError = {
	code: number;
	message: string;
	data?: any;
};

type JsonRpcParams = Record<any, any>;

type JsonRpcBaseInput = JsonRpcCommon & {
	method: string;
	params?: JsonRpcParams | null;
};

type JsonRpcBaseOutput = JsonRpcCommon & ({
	error: JsonRpcError;
} | {
	result: any;
});

export type JsonRpcResponse = JsonRpcBaseOutput & {
	id: JsonRpcId;
};

export type JsonRpcRequest = JsonRpcBaseInput & {
	id: JsonRpcId;
};

export type JsonRpcServerEvent = JsonRpcBaseOutput;
export type JsonRpcClientEvent = JsonRpcBaseInput;

export type JsonRpcCallObject = JsonRpcClientEvent | JsonRpcRequest;
export type JsonRpcResponseObject = JsonRpcResponse | JsonRpcServerEvent;

export type JsonRpcOutput = JsonRpcResponseObject | JsonRpcResponseObject[];

type ValidationResult = {
	isValid: false;
	data: JsonRpcOutput;
} | {
	isValid: true;
	data: CallObjectValidationResult[];
	isBatch: boolean;
};

type CallObjectValidationResult = {
	isValid: false;
	data: JsonRpcResponseObject;
} | {
	isValid: true;
	data: JsonRpcCallObject;
};

export const JSON_RPC_METHOD_SYMBOL = Symbol('json.rpc.method');
export const JSON_RPC_ID_SYMBOL = Symbol('json.rpc.id');

export class JsonRpcProtocol extends BaseProtocol {
	public async handle(
		rawData: unknown,
		handlerHelper: HandlerHelper,
		transform: (data: any) => any,
	): Promise<JsonRpcOutput | undefined> {
		const validationResult = this.validate(rawData);

		if (!validationResult.isValid) {
			return validationResult.data;
		}

		const { isBatch, data } = validationResult;
		type Results = (JsonRpcResponseObject | null)[];

		const results: Results = await Promise.all(data.map(async validationResult => this.handleOne(
			validationResult,
			handlerHelper,
			transform,
		)));
		const filteredResults = results.filter((result): result is JsonRpcResponseObject => result !== null);

		if (!isBatch) {
			return filteredResults[0];
		}

		return filteredResults.length > 0
			? filteredResults
			// eslint-disable-next-line no-undefined
			: undefined;
	}

	public async formatEvent(event: string, data: unknown): Promise<JsonRpcClientEvent> {
		return {
			jsonrpc: '2.0',
			method: event,
			params: data as any,
		};
	}

	private validate(data: any): ValidationResult {
		if (
			typeof data !== 'object'
				|| data === null
				|| (Array.isArray(data) && data.length === 0)
		) {
			return {
				isValid: false,
				data: JsonRpcProtocol.getInvalidRequestOutput(),
			};
		}

		const callObjects = Array.isArray(data)
			? data
			: [data];
		const validationResults = callObjects.map(callObject => this.validateCallObject(callObject));

		return {
			data: validationResults,
			isValid: true,
			isBatch: Array.isArray(data),
		};
	}

	private validateCallObject(data: any): CallObjectValidationResult {
		if (
			typeof data !== 'object'
			|| data === null
			|| typeof data.method !== 'string'
			|| (data.id && typeof data.id !== 'string' && typeof data.id !== 'number')
			|| data.jsonrpc !== '2.0'
			|| (data.params && typeof data.params !== 'object')
		) {
			return {
				isValid: false,
				data: JsonRpcProtocol.getInvalidRequestOutput(),
			};
		}

		return {
			isValid: true,
			data: {
				jsonrpc: data.jsonrpc,
				id: data.id,
				method: data.method,
				params: data.params,
			},
		};
	}

	private async handleOne(
		validationResult: CallObjectValidationResult,
		handlerHelper: HandlerHelper,
		transform: (data: any) => any,
	): Promise<JsonRpcResponseObject | null> {
		if (!validationResult.isValid) {
			return validationResult.data;
		}

		const { data } = validationResult;
		const id: JsonRpcId = (data as any).id || null;
		const baseResult: JsonRpcCommon | JsonRpcResponse = {
			id,
			jsonrpc: '2.0',
		};

		const params: JsonRpcParams = data.params || {};

		(params as any)[JSON_RPC_METHOD_SYMBOL] = data.method;
		(params as any)[JSON_RPC_ID_SYMBOL] = id;

		try {
			const result = await this.call(handlerHelper, data.method, params, transform);

			if (id === null) {
				return null;
			}

			return {
				...baseResult,
				result,
			};
		} catch (error: unknown) {
			const formattedError = this.handleError(error);

			return {
				...baseResult,
				error: formattedError,
			};
		}
	}

	private handleError(error: any): JsonRpcError {
		/* eslint-disable prefer-destructuring */
		let code: number;
		let message: string;

		if (error instanceof BaseException) {
			// eslint-disable-next-line @typescript-eslint/no-unsafe-return
			return error.toJSON() as any;
		}

		if (error instanceof WsException) {
			code = -32000;
			message = error.message;
		} else {
			code = -32603;
			message = error instanceof Error
				? error.message
				: 'Internal error';
		}
		/* eslint-enable prefer-destructuring */

		return { code, message };
	}

	private static getInvalidRequestOutput(): JsonRpcResponseObject {
		return {
			id: null,
			jsonrpc: '2.0',
			error: {
				code: -32600,
				message: 'Invalid Request',
			},
		};
	}
}
