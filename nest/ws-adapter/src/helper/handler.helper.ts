import type { MessageMappingProperties } from '@nestjs/websockets';

import type {
	AdapterOptions,
	CommonHandlerOptions,
	GatewayOptions,
	Pattern,
} from '../adapter/options';

type HandlersMap = Map<Pattern, MessageMappingProperties[]>;

export class HandlerHelper {
	private readonly adapterOptions: Required<AdapterOptions>;

	private readonly handlersMap: HandlersMap = new Map();

	public constructor(adapterOptions: Required<AdapterOptions>) {
		this.adapterOptions = adapterOptions;
	}

	public addGateway(
		handlers: MessageMappingProperties[],
		gatewayOptions: Required<GatewayOptions>,
	): void {
		handlers.forEach(handler => {
			const pattern = this.getHandlerPattern(handler, gatewayOptions);
			const patternHandlers = this.handlersMap.get(pattern) || [];

			patternHandlers.push(handler);
			this.handlersMap.set(pattern, patternHandlers);
		});
	}

	public getPatternHandlers(pattern: string): MessageMappingProperties[] {
		const handlersPatterns = [...this.handlersMap.keys()];

		const stringHandlers = this.handlersMap.get(pattern) || [];
		const regexHandlers = handlersPatterns
			.filter((handlerPattern): handlerPattern is RegExp => typeof handlerPattern !== 'string')
			.filter(handlerPattern => handlerPattern.test(pattern))
			// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
			.map(handlerPattern => this.handlersMap.get(handlerPattern)!)
			.flat();

		return [...stringHandlers, ...regexHandlers];
	}

	private getHandlerPattern(handler: MessageMappingProperties, gatewayOptions: Required<GatewayOptions>): Pattern {
		const basePattern = [...gatewayOptions.namespaces, ''].join(this.adapterOptions.namespaceDelimiter);

		if (HandlerHelper.isStringMethod(handler)) {
			return `${basePattern}${HandlerHelper.getHandlerMethod(handler)}`;
		}

		const regex = handler.message instanceof RegExp
			? handler.message
			: (handler.message as CommonHandlerOptions).method as RegExp;

		return new RegExp(`${basePattern}${regex.source}`, 'u');
	}

	private static getHandlerMethod({ message }: MessageMappingProperties): string {
		if (typeof message === 'string') {
			return message;
		}

		if (message instanceof RegExp || (message as CommonHandlerOptions).method instanceof RegExp) {
			return '';
		}

		return (message as CommonHandlerOptions).method as string;
	}

	private static isStringMethod(handler: MessageMappingProperties): boolean {
		return typeof handler.message === 'string' || typeof (handler.message as CommonHandlerOptions).method === 'string';
	}
}
