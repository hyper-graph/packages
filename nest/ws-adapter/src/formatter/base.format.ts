import {
	InvalidFormatException,
	UnsupportedFormatException,
} from './exceptions';

export abstract class BaseFormat {
	public abstract parse(data: ArrayBuffer | string): Promise<unknown>;

	public abstract format(data: unknown): Promise<ArrayBuffer | string>;

	protected unsupportedFormat(): never {
		throw new UnsupportedFormatException();
	}

	protected parseError(error: unknown): never {
		throw new InvalidFormatException(error);
	}
}
