import { BaseFormat } from './base.format';

export class JsonFormat extends BaseFormat {
	private readonly textDecoder = new TextDecoder();
	// eslint-disable-next-line consistent-return
	public async parse(data: ArrayBuffer | string): Promise<unknown> {
		const jsonString = typeof data === 'string'
			? data
			: this.textDecoder.decode(data);

		try {
			return JSON.parse(jsonString) as unknown;
		} catch (error: unknown) {
			this.parseError(error);
		}
	}

	public async format(data: unknown): Promise<string> {
		return JSON.stringify(data);
	}
}
