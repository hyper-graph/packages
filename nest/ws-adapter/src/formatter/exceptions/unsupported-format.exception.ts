import { BaseException } from '@hg/exception';

export class UnsupportedFormatException extends BaseException {
	public constructor() {
		super('Frame type not supported');
	}
}
