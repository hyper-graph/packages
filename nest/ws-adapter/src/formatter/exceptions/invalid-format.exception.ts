import { BaseException } from '@hg/exception';

export class InvalidFormatException extends BaseException {
	public readonly parseError: unknown;

	public constructor(parseError: unknown) {
		super('Parse error');
		this.parseError = parseError;
	}
}
