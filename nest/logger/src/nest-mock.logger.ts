import { BaseInjectable } from '@hg-nest/common';
import type { LoggerService } from '@nestjs/common';

export class NestMockLogger extends BaseInjectable implements LoggerService {
	public debug(): void {}
	public error(): void {}
	public log(): void {}
	public verbose(): void {}
	public warn(): void {}
}
