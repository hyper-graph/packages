import {
	Global,
	Module,
} from '@nestjs/common';

import { Logger } from './logger';
import { NestWrappedLogger } from './nest-wrapped.logger';
import { WinstonLogger } from './winston.logger';

@Global()
@Module({
	providers: [
		{
			provide: Logger,
			useClass: WinstonLogger,
		},
		NestWrappedLogger,
	],
	exports: [Logger, NestWrappedLogger],
})
export class LoggerModule {
	//
}
