import {
	Inject,
	Injectable,
	Optional,
	Scope,
} from '@nestjs/common';
import { MESSAGE } from 'triple-beam';
import { format } from 'util';
import winston from 'winston';
import { Syslog } from 'winston-syslog';

import { colorize, COLOR } from './colors';
import { Logger } from './logger';
import { LoggerConfig } from './logger.config';

const colorsMap = new Map<string, COLOR>()
	.set('emerg', COLOR.RED)
	.set('alert', COLOR.RED)
	.set('crit', COLOR.RED)
	.set('error', COLOR.RED)
	.set('warning', COLOR.YELLOW)
	.set('notice', COLOR.GREEN)
	.set('info', COLOR.CYAN_BRIGHT)
	.set('debug', COLOR.MAGENTA_BRIGHT);

const colorizeFormat = winston.format(info => {
	const levelColor = colorsMap.get(info.level)!;
	if (info.context) {
		info.context = colorize(COLOR.ORANGE, info.context);
	}

	info.level = colorize(levelColor, info.level);
	info.message = colorize(levelColor, info.message);
	return info;
});

const wrapperFormat = winston.format(info => {
	if (info.context) {
		// eslint-disable-next-line @typescript-eslint/restrict-template-expressions
		info.context = `[${info.context}]`;
	}
	info.level = `<${info.level}>`;
	return info;
});

const processInfoFormat = winston.format(info => {
	if (process.title.endsWith('node')) {
		info.process = `[pid: ${process.pid}]`;
	} else {
		info.process = `${process.title}[${process.pid}]`;
	}

	return info;
});

const timeFormat = winston.format(info => {
	info.timestamp = new Date().toLocaleString('ru-RU');
	return info;
});

const customFormat = winston.format(info => {
	const { process, timestamp, context, level, message } = info;
	const metaInfoItems: string[] = [timestamp, context, level, process];

	const metaInfo = metaInfoItems.join(' ');

	const resultMessage = `${metaInfo}: ${message}`;

	return {
		...info,
		[MESSAGE]: resultMessage,
	};
});

function createSyslogTransport(config: Required<LoggerConfig>): winston.transport {
	return new Syslog({
		protocol: config.syslogPath ? 'unix-connect' : 'tls4',
		// eslint-disable-next-line no-undefined
		path: config.syslogPath ?? undefined,
		// eslint-disable-next-line no-undefined
		host: config.syslogHost ?? undefined,
		// eslint-disable-next-line no-undefined
		port: config.syslogPort ?? undefined,
		facility: config.syslogFacility,
		format: winston.format.simple(),
	});
}

const consoleTransport = new winston.transports.Console({
	format: winston.format.combine(
		colorizeFormat(),
		wrapperFormat(),
		processInfoFormat(),
		timeFormat(),
		customFormat(),
	),
});

@Injectable({ scope: Scope.TRANSIENT })
export class WinstonLogger extends Logger {
	@Inject()
	@Optional()
	private readonly config?: LoggerConfig;
	private context: Nullable<string> = null;
	private winston: winston.Logger;

	public emerg(...args: unknown[]): void {
		this.log('emerg', ...args);
	}

	public alert(...args: unknown[]): void {
		this.log('alert', ...args);
	}

	public crit(...args: unknown[]): void {
		this.log('crit', ...args);
	}

	public error(...args: unknown[]): void {
		this.log('error', ...args);
	}

	public warning(...args: unknown[]): void {
		this.log('warning', ...args);
	}

	public notice(...args: unknown[]): void {
		this.log('notice', ...args);
	}

	public info(...args: unknown[]): void {
		this.log('info', ...args);
	}

	public debug(...args: unknown[]): void {
		this.log('debug', ...args);
	}

	public setContext(context: Nullable<string>): void {
		this.context = context;
	}

	protected async handleModuleInit(): Promise<void> {
		const config = this.getConfig();

		const transports: winston.transport[] = [createSyslogTransport(config)];

		if (config.enableStdout) {
			transports.push(consoleTransport);
		}

		this.winston = winston.createLogger({
			transports,
			levels: winston.config.syslog.levels,
			level: 'debug',
		});
	}

	private log(level: string, ...params: unknown[]): void {
		this.winston.log({
			level,
			message: format(...params),
			context: this.context,
		});
	}

	private getConfig(): Required<LoggerConfig> {
		return {
			enableStdout: true,
			syslogHost: null,
			syslogPort: null,
			syslogPath: null,
			syslogFacility: 'hyper_graph',
			...(this.config || {}),
		};
	}
}
