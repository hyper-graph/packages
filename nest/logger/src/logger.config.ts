export abstract class LoggerConfig {
	public abstract enableStdout?: boolean;
	public abstract syslogHost?: Nullable<string>;
	public abstract syslogFacility?: string;
	public abstract syslogPath?: Nullable<string>;
	public abstract syslogPort?: Nullable<number>;
}
