import { BaseInjectable } from '@hg-nest/common';

export abstract class Logger extends BaseInjectable {
	public abstract emerg(...args: unknown[]): void;
	public abstract alert(...args: unknown[]): void;
	public abstract crit(...args: unknown[]): void;
	public abstract error(...args: unknown[]): void;
	public abstract warning(...args: unknown[]): void;
	public abstract notice(...args: unknown[]): void;
	public abstract info(...args: unknown[]): void;
	public abstract debug(...args: unknown[]): void;
	public abstract setContext(context: Nullable<string>): void;
}
