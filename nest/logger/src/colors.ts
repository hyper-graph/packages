const isColorAllowed = !process.env.NO_COLOR;

export enum COLOR {
	ORANGE = '38;5;3',
	GREEN = '32',
	YELLOW = '33',
	RED = '31',
	MAGENTA_BRIGHT = '95',
	CYAN_BRIGHT = '96',
}

export function colorize(color: COLOR, text: string): string {
	if (!isColorAllowed) {
		return text;
	}

	return `\x1B[${color}m${text}\x1B[39m`;
}
