import { Logger } from './logger';

export class MockLogger extends Logger {
	public alert(): void {}
	public crit(): void {}
	public debug(): void {}
	public emerg(): void {}
	public error(): void {}
	public info(): void {}
	public notice(): void {}
	public warning(): void {}

	public setContext(): void {}
}
