import { BaseInjectable } from '@hg-nest/common';
import { Logger } from './logger';

// eslint-disable-next-line prefer-arrow-callback
BaseInjectable.moduleInitHooks.push(async function(this: BaseInjectable): Promise<void> {
	const { logger } = this as any;

	if (logger instanceof Logger) {
		logger.setContext(Object.getPrototypeOf(this).constructor.name);
	}
});
