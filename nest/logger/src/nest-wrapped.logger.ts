import { BaseInjectable } from '@hg-nest/common';
import {
	LoggerService,
	Logger as NestLogger,
	Inject,
} from '@nestjs/common';
import { Logger } from './logger';

/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
export class NestWrappedLogger extends BaseInjectable implements LoggerService {
	@Inject()
	protected readonly logger: Logger;

	public debug(message: any, context?: string): any {
		this.setOptionalContext(context);
		this.logger.debug(message);
	}

	public error(message: any, trace?: string, context?: string): any {
		this.setOptionalContext(context);
		this.logger.error(message, trace);
	}

	public log(message: any, context?: string): any {
		this.setOptionalContext(context);
		this.logger.info(message);
	}

	public verbose(message: any, context?: string): any {
		this.setOptionalContext(context);
		this.logger.notice(message);
	}

	public warn(message: any, context?: string): any {
		this.setOptionalContext(context);
		this.logger.warning(message);
	}

	protected async handleModuleInit(): Promise<void> {
		this.logger.setContext(null);
	}

	protected async handleApplicationBootstrap(): Promise<void> {
		NestLogger.overrideLogger(true);
		NestLogger.overrideLogger(['debug', 'log', 'verbose', 'warn', 'error']);
		NestLogger.overrideLogger(this);
	}

	private setOptionalContext(context: Nullable<string> = null): void {
		this.logger.setContext(context);
	}
}
