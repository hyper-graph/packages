import type { ModulesContainer } from '@nestjs/core';
import type { InstanceWrapper } from '@nestjs/core/injector/instance-wrapper';

export function getAllProviders(container: ModulesContainer): InstanceWrapper[] {
	return [...container.values()].flatMap(module => [...module.providers.values()]);
}
