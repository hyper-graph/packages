// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function getInstancePrototype(instance: any): Nullable<Record<any, any>> {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return Object.getPrototypeOf(instance);
}
