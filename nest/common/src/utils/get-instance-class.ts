import { getInstancePrototype } from './get-instance-prototype';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function getInstanceClass(instance: any): Function | null {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return getInstancePrototype(instance)?.constructor ?? null;
}
