import type {
	OnApplicationBootstrap,
	OnApplicationShutdown,
	OnModuleDestroy,
	OnModuleInit,
} from '@nestjs/common';

type LifecycleHook = (this: BaseInjectable) => Promise<void>;

export abstract class BaseInjectable
implements OnApplicationBootstrap, OnModuleInit, OnModuleDestroy, OnApplicationShutdown {
	private moduleInitPromise: Promise<void> | null = null;

	private applicationBootstrapPromise: Promise<void> | null = null;

	private applicationShutdownPromise: Promise<void> | null = null;

	private moduleDestroyPromise: Promise<void> | null = null;

	public static readonly moduleInitHooks: LifecycleHook[] = [];
	public static readonly applicationBootstrapHooks: LifecycleHook[] = [];
	public static readonly applicationShutdownHooks: LifecycleHook[] = [];
	public static readonly moduleDestroyHooks: LifecycleHook[] = [];

	public async onModuleInit(): Promise<void> {
		if (!this.moduleInitPromise) {
			this.moduleInitPromise = this.internalHandleModuleInit();
		}

		return this.moduleInitPromise;
	}

	public async onApplicationBootstrap(): Promise<void> {
		if (!this.applicationBootstrapPromise) {
			this.applicationBootstrapPromise = this.internalHandleApplicationBootstrap();
		}

		return this.applicationBootstrapPromise;
	}

	public async onApplicationShutdown(signal?: string): Promise<void> {
		if (!this.applicationShutdownPromise) {
			this.applicationShutdownPromise = this.internalHandleApplicationShutdown(signal);
		}

		return this.applicationShutdownPromise;
	}

	public async onModuleDestroy(): Promise<void> {
		if (!this.moduleDestroyPromise) {
			this.moduleDestroyPromise = this.internalHandleModuleDestroy();
		}

		return this.moduleDestroyPromise;
	}

	protected async handleModuleInit(): Promise<void> {
		await this.callHooks(BaseInjectable.moduleInitHooks);
	}

	protected async handleApplicationBootstrap(): Promise<void> {
		await this.callHooks(BaseInjectable.applicationBootstrapHooks);
	}

	protected async handleApplicationShutdown(_signal?: string): Promise<void> {
		await this.callHooks(BaseInjectable.applicationShutdownHooks);
	}

	protected async handleModuleDestroy(): Promise<void> {
		await this.callHooks(BaseInjectable.moduleDestroyHooks);
	}

	private async internalHandleModuleInit(): Promise<void> {
		await this.callHooks(BaseInjectable.moduleInitHooks);
		await this.handleModuleInit();
	}

	private async internalHandleApplicationBootstrap(): Promise<void> {
		await this.callHooks(BaseInjectable.applicationBootstrapHooks);
		await this.handleApplicationBootstrap();
	}

	private async internalHandleApplicationShutdown(signal?: string): Promise<void> {
		await this.callHooks(BaseInjectable.applicationShutdownHooks);
		await this.handleApplicationShutdown(signal);
	}

	private async internalHandleModuleDestroy(): Promise<void> {
		await this.callHooks(BaseInjectable.moduleDestroyHooks);
		await this.handleModuleDestroy();
	}

	private async callHooks(hooks: LifecycleHook[]): Promise<void> {
		await Promise.all(hooks.map(async hook => hook.call(this)));
	}
}
