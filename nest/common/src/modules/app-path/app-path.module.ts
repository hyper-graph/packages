import {
	DynamicModule,
	Global,
	Module,
} from '@nestjs/common';
import path from 'path';
import { appPathSymbol } from './consts';

@Global()
@Module({
	providers: [
		{
			provide: appPathSymbol,
			useValue: path.resolve(),
		},
	],
	exports: [appPathSymbol],
})
export class AppPathModule {
	public static forRoot(appPath = path.resolve()): DynamicModule {
		return {
			module: AppPathModule,
			providers: [
				{
					provide: appPathSymbol,
					useValue: appPath,
				},
			],
			exports: [appPathSymbol],
			global: true,
		};
	}
}
