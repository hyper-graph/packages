# Hyper graph packages

## Requirements

### Dependencies
* nvm   - v0.38.0
* node  - v16.3.0
* npm   - v7.15.0
* yarn  - v1.22.10

### Environment variables
See [sample](.env.sample) for more info

## Setup
### NodeJS
Set NodeJS version
```shell
nvm use
```

### Npm & Yarn
Install latest npm & yarn versions
```shell
npm i -g npm yarn
```

### Initialize
Will generate script with all env variables
```shell
yarn initialize
```

Manually add env in current shell
```shell
source ./init-env.sh
```

### Install dependencies
```shell
yarn
```

## Scripts

### Build all packages
```shell
yarn build
```

### Lint all packages
```shell
yarn lint
```

### Run tests in all packages
```shell
yarn test
```
