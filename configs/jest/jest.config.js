const ignorePatterns = [
	'/node_modules/',
	'/dist/',
];

const config = {
	cacheDirectory: '.jest-cache',
	clearMocks: true,
	coverageDirectory: '.coverage',
	errorOnDeprecated: true,
	globals: {
		'ts-jest': {
			diagnostics: true,
			useESM: true,
		},
	},
	extensionsToTreatAsEsm: ['.ts'],
	injectGlobals: false,
	coverageProvider: 'v8',
	transform: {},
	preset: 'ts-jest/presets/js-with-ts-esm',
	testEnvironment: 'node',
	testMatch: [
		'**/__tests__/*.ts?(x)',
		'**/?(*.)+(spec|test).ts?(x)',
	],
	transformIgnorePatterns: ignorePatterns,
	testPathIgnorePatterns: ignorePatterns,
};

export default config;
