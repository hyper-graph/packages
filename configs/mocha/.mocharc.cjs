module.exports = {
	'async-only': true,
	extension: [
		'ts',
	],
	spec: [
		'src/**/*.test.ts',
	],
	'trace-warnings': true,
};
