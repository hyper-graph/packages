import { Module } from '@nestjs/common';
import { ProgramModule } from '../program';

import {
	BuildService,
	LinterService,
	NpmService,
	TestService,
} from './services';

const commandService = [BuildService, LinterService, TestService, NpmService];

@Module({
	imports: [ProgramModule],
	providers: commandService,
})
export class CommandsModule {}
