import { Inject } from '@nestjs/common';
import { Command } from 'nestjs-command';

import {
	EslintProgram,
	MochaProgram,
	TypescriptProgram,
} from '../../program';

export class NpmService {
	@Inject()
	protected readonly typescriptProgram: TypescriptProgram;
	@Inject()
	protected readonly jestProgram: MochaProgram;
	@Inject()
	protected readonly eslintProgram: EslintProgram;

	@Command({ command: 'prepublishOnly' })
	public async prepublishOnly(): Promise<number> {
		/* eslint-disable array-element-newline */
		const processes = [
			this.typescriptProgram.exec({ clean: true }),
			this.eslintProgram.exec({}),
			this.jestProgram.exec({}),
		];
		/* eslint-enable array-element-newline */

		const exitCodes = await Promise.all(processes);

		return Number(exitCodes.some(code => code !== 0));
	}
}
