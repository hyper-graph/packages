import { Inject } from '@nestjs/common';
import {
	Command,
	Option,
} from 'nestjs-command';

import { MochaProgram } from '../../program';

const PatternOption = Option({
	name: 'pattern',
	alias: 'p',
	type: 'string',
	requiresArg: false,
});

export class TestService {
	@Inject()
	protected readonly mochaProgram: MochaProgram;

	@Command({ command: 'test' })
	public async test(@PatternOption pattern?: string): Promise<number> {
		return this.mochaProgram.exec({ pattern });
	}
}
