import { Inject } from '@nestjs/common';
import { Command } from 'nestjs-command';

import { EslintProgram } from '../../program';

export class LinterService {
	@Inject()
	protected readonly eslintProgram: EslintProgram;

	@Command({ command: 'lint' })
	public async lint(): Promise<number> {
		return this.eslintProgram.exec({});
	}

	@Command({ command: 'lint:fix' })
	public async lintFix(): Promise<number> {
		return this.eslintProgram.exec({ fix: true });
	}
}
