import {
	Global,
	Module,
} from '@nestjs/common';
import { TypescriptConfig } from './typescript.config';

const configs = [TypescriptConfig];

@Global()
@Module({
	providers: [TypescriptConfig],
	exports: configs,
})
export class ConfigModule {}
