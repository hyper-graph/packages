import { Module } from '@nestjs/common';

import { ConfigModule } from '../config';

import {
	EslintProgram,
	MochaProgram,
	TypescriptProgram,
	TypescriptWatchProgram,
} from './programs';

const programs = [
	EslintProgram,
	TypescriptProgram,
	TypescriptWatchProgram,
	MochaProgram,
];

@Module({
	imports: [ConfigModule],
	providers: programs,
	exports: programs,
})
export class ProgramModule {}
