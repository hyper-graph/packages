import { Logger } from '@nestjs/common';
import { resolve } from 'path';

import { exec } from '../../../utils';

export abstract class BaseProgram<OptionsType = {}> {
	private readonly logger = new Logger();

	public async exec(options: OptionsType): Promise<number> {
		const scriptPath = this.getScriptPath();
		const scriptArgs = [scriptPath, ...this.getArgs(options)];
		const env = this.getEnvs(options);

		await this.beforeExec(options);

		const exitCode = await exec(process.execPath, scriptArgs, env);

		await this.afterExec(options, exitCode);

		return exitCode;
	}

	protected async beforeExec(_options: OptionsType): Promise<void> {}
	protected async afterExec(_options: OptionsType, exitCode: number): Promise<void> {
		this.logger.log(`Process exit with code ${exitCode}`, Object.getPrototypeOf(this).constructor.name);
	}

	protected getEnvs(_options: OptionsType): Record<string, string> {
		return {};
	}

	protected abstract getArgs(options: OptionsType): string[];
	protected abstract getCommandName(): string;

	private getScriptPath(): string {
		return resolve(__dirname, '..', '..', '..', 'bin', `${this.getCommandName()}.js`);
	}
}
