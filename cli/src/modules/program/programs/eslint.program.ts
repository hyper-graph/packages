import { Inject } from '@nestjs/common';

import { TypescriptConfig } from '../../config';
import { BaseProgram } from './base.program';

type EslintOptions = {
	fix?: boolean;
};

export class EslintProgram extends BaseProgram<EslintOptions> {
	@Inject()
	protected typescriptConfig: TypescriptConfig;

	protected getArgs(options: EslintOptions): string[] {
		const { fix } = options;

		const args = ['--ext', '.ts,.tsx', '--format', 'codeframe', '--cache'];

		if (fix) {
			args.push('--fix');
		}

		return [...args, this.typescriptConfig.rootDir];
	}

	protected getCommandName(): string {
		return 'eslint';
	}
}
