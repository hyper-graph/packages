import { Inject } from '@nestjs/common';
import { rm } from 'fs/promises';
import { TypescriptConfig } from '../../config';
import { BaseProgram } from './base.program';

export type TypescriptOptions = {
	clean?: boolean;
	commonjs?: boolean;
};

export abstract class BaseTypescriptProgram<AdditionalOptionType = {}>
	extends BaseProgram<AdditionalOptionType & TypescriptOptions> {
	@Inject()
	protected readonly typescriptConfig: TypescriptConfig;

	protected async beforeExec(options: TypescriptOptions): Promise<void> {
		const { clean } = options;
		const { outDir } = this.typescriptConfig;

		if (clean) {
			await rm(outDir, {
				recursive: true,
				force: true,
			});
		}
	}

	protected getCommonArgs(options: TypescriptOptions): string[] {
		const { commonjs = false } = options;

		return [
			'--project',
			commonjs
				? this.typescriptConfig.esmPath
				: this.typescriptConfig.cjsPath!,
		];
	}
}
