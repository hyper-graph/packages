import {
	BaseTypescriptProgram,
	TypescriptOptions,
} from './base.typescript.program';

type Options = {
	onSuccess?: string;
}
export class TypescriptWatchProgram extends BaseTypescriptProgram<Options> {
	protected getArgs(options: Options & TypescriptOptions): string[] {
		const { onSuccess } = options;
		const args = [...this.getCommonArgs(options), '--noClear'];

		if (onSuccess) {
			args.push('--onSuccess', onSuccess);
		}

		return args;
	}

	protected getEnvs(): Record<string, string> {
		return { NODE_OPTIONS: '' };
	}

	protected getCommandName(): string {
		return 'typescript-watch';
	}
}
