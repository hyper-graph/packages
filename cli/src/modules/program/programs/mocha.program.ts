import isCi from 'is-ci';
import { resolve } from 'path';

import { BaseProgram } from './base.program';

type JestOptions = {
	coverage?: boolean;
	pattern?: string;
};

type JunitReporterOptions = {
	testCaseSwitchClassnameAndName?: boolean;
	mochaFile: string;
};

export class MochaProgram extends BaseProgram<JestOptions> {
	protected getArgs(options: JestOptions): string[] {
		const { pattern } = options;

		const args = ['--passWithNoTests'];

		if (isCi) {
			this.getReporterArguments(args);
		}

		if (pattern) {
			args.push(pattern);
		}

		return args;
	}

	protected getEnvs(_options: JestOptions): Record<string, string> {
		const nodeArgs = [
			'--enable-source-maps',
			'--experimental-import-meta-resolve',
			'--experimental-json-modules',
			'--abort-on-uncaught-exception',
			'--experimental-specifier-resolution=node',
			'--no-warnings',
			'--loader=ts-node/esm',
		];
		return {
			HG_ENV: 'test',
			NODE_OPTIONS: nodeArgs.join(' '),
		};
	}

	protected getCommandName(): string {
		return 'mocha';
	}

	private getReporterArguments(args: string[]): void {
		args.push(
			'--reporter',
			'mocha-junit-reporter',
		);

		const options: JunitReporterOptions = { mochaFile: resolve('junit.xml') };

		const optionNames = Object.keys(options) as (keyof JunitReporterOptions)[];

		optionNames.forEach(key => {
			const value = options[key];

			if (!value) {
				return;
			}

			args.push(
				'--reporter-options',
				`${key}=${String(value)}`,
			);
		});
	}
}
