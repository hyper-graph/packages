#!/usr/bin/env node
import type { LogLevel } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { CommandService } from 'nestjs-command';

import { CliModule } from './cli.module';

async function main(): Promise<void> {
	const logLevels: LogLevel[] = ['debug', 'verbose', 'log', 'warn', 'error'];

	const app = await NestFactory.createApplicationContext(CliModule, { logger: ['warn', 'error'] });

	await app.init();

	if (process.env.HG_DEBUG) {
		app.useLogger(['debug', 'verbose', 'log', 'warn', 'error']);
	}

	const commandService = app.get(CommandService);
	const { yargs } = commandService;

	yargs
		.scriptName('hg')
		.skipValidation('----')
		.option('completion', {
			type: 'boolean',
			default: false,
		})
		.option('log-level', {
			alias: 'l',
			type: 'string',
			requiresArg: false,
			default: 'warn' as LogLevel,
		})
		.coerce('log-level', arg => {
			if (logLevels.includes(arg)) {
				const requestedLogLevels = logLevels.slice(logLevels.indexOf(arg));

				app.useLogger(requestedLogLevels);
			}
		})
		.coerce('completion', arg => {
			if (arg) {
				yargs.showCompletionScript();
				process.exit(0);
			}
		});

	commandService.exec();
}

main().catch(error => {
	console.error(error);

	process.exit(1);
});
