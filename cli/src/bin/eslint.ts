import { resolve } from 'path';
import { getPathToPackage } from '../utils';

async function main(): Promise<void> {
	const pathToPackage = await getPathToPackage('eslint');

	if (pathToPackage === null) {
		console.error('Eslint package not found');

		process.exit(1);
	}
	await import(resolve(pathToPackage, 'bin', 'eslint.js'));
}

main().catch(error => {
	console.log(error);
	process.exit(1);
});
export {};
