import { resolve } from 'path';
import { getPathToPackage } from '../utils';

async function main(): Promise<void> {
	const pathToPackage = await getPathToPackage('typescript');

	if (pathToPackage === null) {
		console.error('Typescript package not found');

		process.exit(1);
	}

	await import(resolve(pathToPackage, 'lib', 'tsc.js'));
}

main().catch(error => {
	console.log(error);
	process.exit(1);
});
export {};
