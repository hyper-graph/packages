import { resolve } from 'path';
import { copyFile } from 'fs/promises';

import { getPathToPackage } from '../utils';

async function main(): Promise<void> {
	const pathToPackage = await getPathToPackage('mocha');

	if (pathToPackage === null) {
		console.error('Jest package not found');

		process.exit(1);
	}
	const execPath = resolve(pathToPackage, 'bin', 'mocha.js');

	await copyFile(resolve(pathToPackage, 'bin', 'mocha'), execPath);

	await import(execPath);
}

main().catch(error => {
	console.log(error);
	process.exit(1);
});
export {};
